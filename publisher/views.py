from django.shortcuts import render,redirect
from .forms import PublisherForm
from django.http import HttpResponse
from accounts.models import *
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator


@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		publisher = Publisher.objects.get(pk=request.POST['pk'])
		publisher.pub_name = request.POST['name']
		publisher.save()
		return redirect('/publisher/')
	publishers = Publisher.objects.all().order_by('pub_name')
	paginator = Paginator(publishers, 10)
	page = request.GET.get('page')
	publishers = paginator.get_page(page)
	return render(request,'publisher/index.html',{'publishers':publishers,'paginationdata':publishers})

@login_required(login_url='/login/')
def addpublisher(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		publisherform = PublisherForm(request.POST)
		if publisherform.is_valid():
			publisherform.save()
			return redirect('/publisher')
		else:
			return HttpResponse(publisherform.errors)
	return render(request,'publisher/addpublisher.html')