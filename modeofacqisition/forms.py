from django.forms import ModelForm
from accounts.models import ModeOfAccuisition

class modeofacquisitionForm(ModelForm):
	class Meta:
		model = ModeOfAccuisition
		fields = '__all__'
