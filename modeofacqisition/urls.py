from django.urls import path
from .import views

app_name = 'modeofacqisition'

urlpatterns = [
	path('',views.index, name='index'),
	path('modeofacqisitionadd',views.modeofacqisitionadd,name='modeofacqisitionadd'),
]