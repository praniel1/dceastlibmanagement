from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import modeofacquisitionForm
from accounts.models import ModeOfAccuisition
from django.contrib.auth.decorators import login_required

def index(request):
	if request.method == 'POST':
		modeofacquisition = ModeOfAccuisition.objects.get(pk=request.POST['pk'])
		modeofacquisition.accuisition_name = request.POST['name']
		modeofacquisition.save()
		return redirect('/modeofacqisition/')
	accuisitionmodes = ModeOfAccuisition.objects.all().order_by('-pk')
	return render(request,'modeofacqisition/index.html',{'accuisitionmodes':accuisitionmodes})

def modeofacqisitionadd(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		form = modeofacquisitionForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/modeofacqisition/')
		else:
			return HttpResponse(form.errors)
	else:
		return render(request,'modeofacqisition/addacqisition.html')