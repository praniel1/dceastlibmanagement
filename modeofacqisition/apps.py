from django.apps import AppConfig


class ModeofacqisitionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modeofacqisition'
