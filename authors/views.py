from django.shortcuts import render, redirect
from accounts.models import Author
from django.http import HttpResponse
from .forms import AuthorForm
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator

@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		author = Author.objects.get(pk=request.POST['pk'])
		author.author_name = request.POST['name']
		author.save()
		return redirect('/authors/')
	authors = Author.objects.all().order_by('author_name')
	paginator = Paginator(authors, 10)
	page = request.GET.get('page')
	authors = paginator.get_page(page)
	context = {'authors':authors,'paginationdata':authors}
	return render(request,'authors/index.html',context)

@login_required(login_url='/login/')
def addauthor(request):
	if request.method=='POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		authorform = AuthorForm(request.POST)
		if authorform.is_valid():
			authorform.save()
			return redirect('/authors/')
		else:
			return HttpResponse(authorform.errors)
	return render(request,'authors/addauthor.html')