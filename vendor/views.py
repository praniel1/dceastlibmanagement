from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import VendorForm
from accounts.models import Vendor
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		vendor = Vendor.objects.get(pk=request.POST['pk'])
		vendor.vendor_name = request.POST['name']
		vendor.save()
		return redirect('/vendor/')
	vendors = Vendor.objects.all().order_by('-pk')
	return render(request,'vendor/index.html',{'vendors':vendors})

@login_required(login_url='/login/')
def addvendor(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		form = VendorForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/vendor/')
		else:
			return HttpResponse(form.errors)
	else:
		return render(request,'vendor/addvendor.html')