from django.forms import ModelForm
from accounts.models import Vendor

class VendorForm(ModelForm):
	class Meta:
		model = Vendor
		fields = '__all__'