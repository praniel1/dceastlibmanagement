from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.contrib import messages
from accounts.models import User, Book, Issue
from books.views import searchbookfunc


def index(request):
	if request.user.is_authenticated:
		return redirect("/")
	return render(request,'loginpage/index.html')

def loginuser(request):
	if request.user.is_authenticated:
		return redirect("/")
	if request.method == 'POST':
		user=authenticate(username=request.POST['username'],password=request.POST['password'])
		if user is not None:
			login(request, user)
			if request.GET.get('next'):
				return redirect(request.GET.get('next'))
			return redirect('/')
		else:
			messages.error(request,'Please check your credentails')
	return render(request,'loginpage/loginuser.html')

def logoutuser(request):
	logout(request)
	return redirect('/')

def createuser(request):
	if request.method == 'POST':
		if request.POST['password1'] != request.POST['password2']:
			messages.error(request,'Passwords dont match')
			return redirect('/login/register')
		if User.objects.filter(username=request.POST['username']):
			messages.error(request,'UserName already present')
			return redirect('/login/register')
		user = User.objects.create_user(username=request.POST['username'], password=request.POST['password1'])
		login(request, user)
		return redirect('/')
	return render(request,'loginpage/createuser.html')

def searchforbook(request):
	books = Book.objects.all().filter(title__icontains=request.GET.get('title'))
	return render(request,'loginpage/searchforbooks.html',{'books':books})

def bookstatus(request,pk):
	book = Book.objects.get(pk=pk)
	issued = Issue.objects.filter(book=book).filter(returned=False)
	if(len(issued)==0):
		issued = 'Available'
	else:
		issued = 'Not Available'
	return render(request,'loginpage/bookstatus.html',{'book':book,'issued':issued})

def changepassword(request):
	if request.method == 'POST':
		if request.POST['password1'] != request.POST['password2']:
			messages.error(request,'Passwords dont match')
		user = User.objects.get(username__exact=request.POST['username'])
		user.set_password(request.POST['password1'])
		user.save()
		messages.success(request,'Password changed')
	return render(request,'loginpage/changepassword.html')