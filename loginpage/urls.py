from django.urls import path
from . import views
app_name = 'login'

urlpatterns = [
	path('', views.index, name='index'),
	path('loginuser', views.loginuser, name='loginuser'),
	path('logoutuser', views.logoutuser, name='logoutuser'),
	path('register', views.createuser, name='createuser'),
	path('searchforbook', views.searchforbook, name='searchforbook'),
	path('bookstatus/<int:pk>', views.bookstatus, name='bookstatus'),
	path('changepassword',views.changepassword, name='changepassword'),
]