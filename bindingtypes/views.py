from django.shortcuts import render,redirect
from django.http import HttpResponse
from accounts.models import BindingType
from .forms import BindingTypeForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def index(request):
	if request.method == "POST":
		bindingtype = BindingType.objects.get(pk=request.POST['pk'])
		bindingtype.bindingtypename = request.POST['name']
		bindingtype.save()
		return redirect('/bindingtypes/')
	bindingtypes = BindingType.objects.all().order_by('-pk')
	return render(request,'bindingtypes/index.html',{'bindingtypes':bindingtypes})

@login_required(login_url='/login/')
def addbinding(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		form = BindingTypeForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/bindingtypes')
		else:
			return HttpResponse(form.errors)
	return render(request,'bindingtypes/addbindingtype.html')