from django.contrib import admin
from accounts.models import BindingType

admin.site.register(BindingType)