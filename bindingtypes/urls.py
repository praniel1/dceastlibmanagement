from django.urls import path
from .import views

app_name='bindingtypes'

urlpatterns = [
	path('',views.index, name='index'),
	path('addbinding',views.addbinding, name='addbinding'),
]