from django.forms import ModelForm
from accounts.models import BindingType

class BindingTypeForm(ModelForm):
	class Meta:
		model = BindingType
		fields = '__all__'