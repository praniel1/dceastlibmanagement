from django.apps import AppConfig


class BindingtypesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bindingtypes'
