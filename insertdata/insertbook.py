import xlrd
import time
import csv
import sys,os
import django

sys.path.append('/home/ubuntu/Desktop/projects/dceastlibmanagement')
# sys.path.append('/home/praniel/Desktop/projects/dceastlibmanagement')
os.environ['DJANGO_SETTINGS_MODULE'] = 'dceastlibmanagement.settings'
django.setup()
from accounts.models import *
from books.forms import BookForm
file = open('combineddata.csv','r')
reader = csv.reader(file)

def findauthor(one,two,three):
	authlist = []
	if len(one)>1:
		authlist.append(one)
	if len(two)>1:
		authlist.append(two)
	if len(three)>1:
		authlist.append(three)
	authlistid = []
	for auth in authlist:
		authorid = Author.objects.filter(author_name__iexact=auth).values_list('pk', flat=True)
		authorid = list(authorid)
		for single in authorid:
			authlistid.append(single)
	return authlistid

def findpublisher(pub):
	publist = []
	if len(pub) == 0:
		return publist
	pub = Publisher.objects.filter(pub_name__iexact=pub).values_list('pk',flat=True)
	pub = list(pub)
	a = 0
	for p in pub:
		a = p
	return a

def findbinding(binding):
	bindlist = []
	if len(binding) == 0:
		return bindlist
	bind = BindingType.objects.filter(bindingtypename=binding).values_list('pk',flat=True)
	bind = list(bind)
	# print(bind)
	# return bind
	a = 0
	for p in bind:
		a = p
	return a

for i,row in enumerate(reader):
	if i == 0:
		continue
	bookdict = {}
	bookdict['title'] = row[5].title()
	bookdict['subtitle'] = row[6]
	bookdict['author'] = findauthor(row[7],row[8],row[9])
	bookdict['edition'] = row[14]
	bookdict['year'] = row[17]
	bookdict['place'] = row[15]
	bookdict['publisher'] = findpublisher(row[16])
	bookdict['num_of_vols'] = row[40]
	# bookdict['accompaning_materials'] = row[5]
	# bookdict['item_price'] = row[5]
	# bookdict['vendor'] = row[5]
	# bookdict['invoice_date'] = row[5]
	# bookdict['invoice_number'] = row[5]
	bookdict['accession_number'] = row[38]
	# bookdict['accession_date'] = row[39]
	# bookdict['doc_type'] = row[5]
	bookdict['location'] = row[48]
	bookdict['type_of_binding'] = findbinding(row[46])
	# bookdict['type_of_collection'] = row[5]
	if bookdict['type_of_binding'] == 0:
		bookdict['type_of_binding'] = ''
	bookdict['added_by'] = User.objects.get(username='praniel')
	bookdict['last_updated_by'] = User.objects.get(username='praniel')

	print(bookdict)
	# time.sleep(0.05)
	form = BookForm(bookdict)
	if form.is_valid():
		form.save()
		print('save')
	else:
		print(form.errors)
		exit()
	# exit()
