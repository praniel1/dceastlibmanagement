import xlrd
import time
import csv

loc = 'combineddata.xls'

wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)

def savetocsv(row):
	row[0] = int(row[0])
	f = open('books.csv','a+')
	writer = csv.writer(f)
	writer.writerow(row)
	f.close()

f = open('books.csv','a+')
writer = csv.writer(f)
writer.writerow(sheet.row_values(0))

for i in range(1,4417):
	for j in range(1,4417):
		if i == int(sheet.cell_value(j,0)):
			row = sheet.row_values(j)
			# print(row)
			# time.sleep(0.5)
			savetocsv(row)