import xlrd
import time
import csv
import sys,os
import django

def savetocsv(row):
	# row[0] = int(row[0])
	f = open('combineddata.csv','a+')
	writer = csv.writer(f)
	writer.writerow(row)
	f.close()

loc = 'dbo_CATS.xls'
wb = xlrd.open_workbook(loc)
sheet_cats = wb.sheet_by_index(0)

loc = 'dbo_HOLDINGS.xls'
wb = xlrd.open_workbook(loc)
sheet_holdings = wb.sheet_by_index(0)

fulldata = []
for i in range(0,4417):
	row = sheet_cats.row_values(i)+sheet_holdings.row_values(i)
	if sheet_cats.row_values(i)[0] != sheet_holdings.row_values(i)[0]:
		print(row)
		break
	fulldata.append(row)

for f in fulldata:
	savetocsv(f)