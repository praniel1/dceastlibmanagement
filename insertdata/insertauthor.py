import xlrd
import time
import csv
import sys,os
import django

loc = 'dbo_CATS.xls'

wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)

finalarray = []

def getauthorname(column):
	for j in range(1,4418):
		row = sheet.row_values(j)
		authorname = row[column]
		if (len(authorname)!=0):
			if authorname not in finalarray:
				finalarray.append(authorname)

getauthorname(7)
getauthorname(8)
getauthorname(9)

# sys.path.append('/home/ubuntu/Desktop/projects/dceastlibmanagement')
sys.path.append('/home/praniel/Desktop/projects/dceastlibmanagement')
os.environ['DJANGO_SETTINGS_MODULE'] = 'dceastlibmanagement.settings'
django.setup()

from accounts.models import Author, User

user = User.objects.get(username='praniel')
for a in finalarray:
	author = Author()
	author.author_name = a
	author.added_by = user
	author.save()