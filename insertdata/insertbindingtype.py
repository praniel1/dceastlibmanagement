# import xlrd
# import time
# import csv
import sys,os
import django

# sys.path.append('/home/ubuntu/Desktop/projects/dceastlibmanagement')
sys.path.append('/home/praniel/Desktop/projects/dceastlibmanagement')
os.environ['DJANGO_SETTINGS_MODULE'] = 'dceastlibmanagement.settings'
django.setup()
from accounts.models import *

types = ['Hard Bound','Paperback','Spiral Binding','Leather Binding']

for t in types:
	form = BindingType()
	form.bindingtypename = t
	form.save()