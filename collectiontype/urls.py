from django.urls import path
from .import views

app_name = 'collectiontype'

urlpatterns = [
	path('',views.index, name='index'),
	path('addcollectiontype', views.addcollectiontype, name='addcollectiontype'),
	
]