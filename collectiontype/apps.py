from django.apps import AppConfig


class CollectiontypeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'collectiontype'
