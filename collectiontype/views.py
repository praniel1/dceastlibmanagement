from django.shortcuts import render, redirect
from .forms import CollectionTypeForm
from django.http import HttpResponse
from accounts.models import TypeOfCollection
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		collectiontype = TypeOfCollection.objects.get(pk=request.POST['pk'])
		collectiontype.collection_name = request.POST['name']
		collectiontype.save()
		return redirect('/collectiontype/')
	collectiontypes = TypeOfCollection.objects.all().order_by('-pk')
	return render(request,'collectiontype/index.html',{'collectiontypes':collectiontypes})

@login_required(login_url='/login/')
def addcollectiontype(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		form = CollectionTypeForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/collectiontype')
		else:
			return HttpResponse(request.errors)
	return render(request,'collectiontype/collectiontypeadd.html')