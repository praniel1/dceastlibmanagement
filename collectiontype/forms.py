from django.forms import ModelForm
from accounts.models import TypeOfCollection

class CollectionTypeForm(ModelForm):
	class Meta:
		model = TypeOfCollection
		fields = '__all__'