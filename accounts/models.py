from django.db import models
from django.contrib.auth.models import AbstractUser,AbstractBaseUser, BaseUserManager
from .ModelManager import UserManager
import datetime

class User(AbstractBaseUser):
	# username=models.CharField(max_length=50)
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	username = models.CharField(max_length=100,unique=True)
	is_active = models.BooleanField(default=True)
	staff = models.BooleanField(default=False)
	USERNAME_FIELD = 'username'
	objects = UserManager()
	@property
	def is_staff(self):
		return self.staff
	@property
	def is_admin(self):
		return self.admin
	def has_perm(self, perm, obj=None):
		return True
	def has_module_perms(self, app_lable):
		return True


	def __str__(self):
		return self.username

class Author(models.Model):
	# dateadded = models.DateField()
	author_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.author_name

class Publisher(models.Model):
	pub_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.pub_name

class AccompanyingMaterials(models.Model):
	accom_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)
	def __str__(self):
		return self.accom_name

class Vendor(models.Model):
	vendor_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.vendor_name

class BindingType(models.Model):
	bindingtypename = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.bindingtypename

class DocumentType(models.Model):
	doctype_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.doctype_name

class TypeOfCollection(models.Model):
	collection_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.collection_name

class ModeOfAccuisition(models.Model):
	accuisition_name = models.CharField(max_length=100)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.accuisition_name

class Book(models.Model):
	title = models.CharField(max_length=500)
	subtitle = models.CharField(max_length=500,blank=True,null=True)
	author = models.ManyToManyField(Author,blank=True)
	edition = models.CharField(max_length=30,blank=True,null=True)
	year = models.IntegerField(null=True,blank=True)
	place = models.CharField(max_length=500)
	publisher = models.ForeignKey(Publisher, on_delete=models.RESTRICT,null=True,blank=True)
	num_of_vols = models.CharField(max_length=20, null=True, blank=True)
	accompaning_materials = models.ForeignKey(AccompanyingMaterials,on_delete=models.RESTRICT,null=True, blank=True)
	item_price = models.CharField(max_length=20,null=True, blank=True)
	vendor = models.ForeignKey(Vendor,on_delete=models.RESTRICT,null=True, blank=True)
	invoice_date = models.DateField(null=True,blank=True)
	invoice_number = models.IntegerField(null=True,blank=True)
	accession_number = models.CharField(max_length=30, null=True, blank=True)
	accession_date = models.DateField(null=True, blank=True)
	type_of_binding = models.ForeignKey(BindingType,on_delete=models.RESTRICT,null=True, blank=True)
	doc_type = models.ForeignKey(DocumentType, on_delete=models.RESTRICT,null=True, blank=True)
	location = models.TextField(null=True, blank=True)
	type_of_collection = models.ForeignKey(TypeOfCollection,on_delete=models.RESTRICT,null=True,blank=True)

	editor = models.CharField(max_length=200,null=True,blank=True)
	ceases_exist_date = models.DateField(null=True,blank=True)
	mode_of_accuisition = models.ForeignKey(ModeOfAccuisition,on_delete=models.RESTRICT,null=True,blank=True)
	remarks = models.TextField(null=True,blank=True)

	ceased = models.BooleanField(default=False)
	date_added = models.DateField(default=datetime.date.today,null=True, blank=True)
	last_updated = models.DateField(null=True,blank=True)
	added_by = models.ForeignKey(User, on_delete=models.RESTRICT, related_name='added_by',null=True, blank=True)
	last_updated_by = models.ForeignKey(User, on_delete=models.RESTRICT, related_name='updated_by')

	def __str__(self):
		return self.title

class Issue(models.Model):
	book = models.ForeignKey(Book,on_delete=models.RESTRICT)
	member_name = models.CharField(max_length=300, null=True,blank=True)
	member_number = models.CharField(max_length=20, null=True,blank=True)
	issue_date = models.DateField()
	return_date = models.DateField(null=True, blank=True)
	issued_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)
	returned = models.BooleanField(default=False)
	remark_return = models.TextField(null=True,blank=True)
	def __str__(self):
		return self.book.title