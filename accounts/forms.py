from .models import User

class UserForm(forms.ModelForm):
	# first_name = forms.CharField()
	# last_name = forms.CharField()
	# email = forms.EmailField()
	class Meta:
		model = User
		# fields = ('first_name','last_name','email','password')
		fields = '__all__'
