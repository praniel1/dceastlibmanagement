from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin

class UserAdmin(admin.ModelAdmin):
	readonly_fields = ['password']

admin.site.register(User, UserAdmin)
admin.site.register(Author)
admin.site.register(Publisher)
admin.site.register(AccompanyingMaterials)
admin.site.register(Vendor)
admin.site.register(DocumentType)
admin.site.register(TypeOfCollection)
admin.site.register(Book)
admin.site.register(Issue)