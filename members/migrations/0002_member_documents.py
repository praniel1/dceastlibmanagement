# Generated by Django 3.2.9 on 2022-09-20 04:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='documents',
            field=models.FileField(blank=True, null=True, upload_to='memberforms/'),
        ),
    ]
