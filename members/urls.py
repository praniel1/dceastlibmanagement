from django.urls import path
from . import views

app_name = 'members'

urlpatterns = [
	path('', views.index,name='index'),
	path('addmember', views.addmember, name='addmember'),
	path('view/<int:pk>',views.viewmember, name='viewmember'),
	path('printcard/<int:pk>',views.printcard,name='printcard'),
]