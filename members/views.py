from django.shortcuts import render, redirect
from .forms import MemberForm
from django.http import HttpResponse
from .models import Member

def index(request):
	if request.method == 'POST':
		member = Member.objects.get(pk=request.POST['pk'])
		member.name = request.POST['name']
		member.regno = request.POST['regno']
		member.save()
	members = Member.objects.all()
	return render(request,'members/index.html',{'members':members})

def addmember(request):
	if request.method == 'POST':
		form = MemberForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/members/')
		else:
			return HttpResponse(form.errors)
	return render(request,'members/addmember.html')

def viewmember(request,pk):
	member = Member.objects.get(pk=pk)
	return render(request,'members/viewmember.html',{'member':member})

def printcard(request,pk):
	member = Member.objects.get(pk=pk)
	return render(request,'members/printforcard.html',{'member':member})	