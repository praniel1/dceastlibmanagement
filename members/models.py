from django.db import models

class Member(models.Model):
	name = models.CharField(max_length=100)
	regno = models.CharField(max_length=100)
	dateofreg = models.DateField()
	validupto = models.DateField()
	photo = models.ImageField(upload_to='pictures/')
	documents = models.FileField(upload_to='memberforms/',null=True,blank=True)
	
	def __str__(self):
		return self.name