from django.shortcuts import render, redirect
from accounts.models import *
from django.contrib.auth.decorators import login_required
from books.forms import IssueForm
from django.http import HttpResponse
from datetime import date
from django.db.models import Q
from django.core.paginator import Paginator
from django.core.paginator import Paginator

@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		issuedbook = Issue.objects.get(pk=request.POST['pk'])
		issuedbook.returned = True
		issuedbook.remark_return = request.POST['remark_return']
		issuedbook.save()
		return redirect('/')
	pastissuedbooks = Issue.objects.filter(returned=False).filter(return_date__lt=date.today())
	todayreturn = Issue.objects.filter(returned=False).filter(return_date=date.today())
	issuedbooks = Issue.objects.filter(returned=False).exclude(return_date=date.today()).exclude(return_date__lt=date.today())
	context = {'issuedbooks':issuedbooks,'todayreturn':todayreturn,'pastissuedbooks':pastissuedbooks}
	return render(request,'homepage/index.html',context)

@login_required(login_url='/login/')
def issuebook(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		# postrequest['issue_date'] = date.today()
		postrequest['issued_by'] = request.user
		form = IssueForm(postrequest)
		if form.is_valid():
			form.save()
		else:
			return HttpResponse(str(form.errors))
		return redirect('/')
	# books = Book.objects.all()
	return render(request,'homepage/issuebook.html')

@login_required(login_url='/login/')
def viewpast(request):
	issuedbooks = Issue.objects.filter(returned=True)
	paginator = Paginator(issuedbooks, 15)
	page = request.GET.get('page')
	issuedbooks = paginator.get_page(page)
	context ={'issuedbooks':issuedbooks,'paginationdata':issuedbooks}
	return render(request,'homepage/pastissuedbooks.html',context)

@login_required(login_url='/login/')
def renewdate(request):
	issuedbook = Issue.objects.get(pk=request.POST['pk'])
	issuedbook.return_date = request.POST['renewdate']
	issuedbook.save()
	return redirect('/')

# @login_required(login_url='/login/')
# def searchbooks(request):
# 	books = Book.objects.all()
# 	paginator = Paginator(books, 12)
# 	page = request.GET.get('page')
# 	books = paginator.get_page(page)
# 	# context = getcontext()
# 	context = {}
# 	context['books'] = context['paginationdata'] = books
# 	return render(request,'homepage/searchbooks.html',{'books':books})

def getcontext():
	context = {
		'authors' : Author.objects.all().order_by('author_name'),
		'accmaterials' : AccompanyingMaterials.objects.all(),
		'vendors' : Vendor.objects.all(),
		'bindingtypes':BindingType.objects.all(),
		'doctypes': DocumentType.objects.all(),
		'typeofcollections' : TypeOfCollection.objects.all(),
		'publishers': Publisher.objects.all(),
		'modeofacquisitions':ModeOfAccuisition.objects.all(),
	}
	return context

def booksearchcondition(request,listofval):
	for l in listofval:
		if request.GET.get(l):
			return True
	return False
	# request.GET.get('author')or request.GET.get('publisher')or request.GET.get('title') or request.GET.get('year')	

@login_required(login_url='/login/')
def searchbooks(request):
	books = Book.objects.all().order_by('accession_number')
	listofval = ['author','publisher','title','year','accession_number']
	if (booksearchcondition(request,listofval)):
		if request.GET.get('title'):
			books = books.filter(title__icontains=request.GET.get('title'))
		if request.GET.get('year'):
			books = books.filter(year__icontains=request.GET.get('year'))
		if request.GET.get('publisher'):
			books = books.filter(publisher__pub_name__icontains=request.GET.get('publisher'))
		if request.GET.get('author'):
			books = books.filter(author__author_name__icontains=request.GET.get('author'))
		if request.GET.get('accession_number'):
			books = books.filter(accession_number__icontains=request.GET.get('accession_number'))
	paginator = Paginator(books, 12)
	page = request.GET.get('page')
	books = paginator.get_page(page)
	context = getcontext()
	context['books'] = context['paginationdata'] = books
	return render(request,'homepage/searchbooks.html',context)