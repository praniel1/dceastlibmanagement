from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
	path('',views.index,name='index'),
	path('issuebook', views.issuebook,name='issuebook'),
	path('viewpast', views.viewpast, name='viewpast'),
	path('renewdate',views.renewdate, name='renewdate'),
	path('searchbooks',views.searchbooks, name='searchbooks'),
]