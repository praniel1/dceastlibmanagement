from django.apps import AppConfig


class AccompanyingmaterialsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'accompanyingmaterials'
