from django.urls import path
from .import views

app_name = 'accompanyingmaterials'

urlpatterns = [
	path('',views.index, name='index'),
	path('addaccompanying',views.addaccompanying,name='addaccompanying'),
	
]