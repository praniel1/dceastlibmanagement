from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import AccomMaterialsForm
from accounts.models import AccompanyingMaterials
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		accommaterial = AccompanyingMaterials.objects.get(pk=request.POST['pk'])
		accommaterial.accom_name = request.POST['name']
		accommaterial.save()
		return redirect('/accompanyingmaterials/')
	accommaterials = AccompanyingMaterials.objects.all().order_by('-pk')
	return render(request,'accompanyingmaterials/index.html',{'accommaterials':accommaterials})

@login_required(login_url='/login/')
def addaccompanying(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		form = AccomMaterialsForm(request.POST)
		if form.is_valid():
			form.save()
			return 	redirect('/accompanyingmaterials/')
		else:
			return HttpResponse(form.errors)
	return render(request,'accompanyingmaterials/addaccompanying.html')