from django.forms import ModelForm
from accounts.models import AccompanyingMaterials

class AccomMaterialsForm(ModelForm):
	class Meta:
		model = AccompanyingMaterials
		fields = '__all__'