import os
import sys

# sys.path.append('/home/ubuntu/Desktop/projects/virtualpython')
# sys.path.append('/home/ubuntu/Desktop/projects/dceastlibmanagement')
# sys.path.append('/home/ubuntu/Desktop/projects/dceastlibmanagement/dceastlibmanagement')
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dceastlibmanagement.settings')

application = get_wsgi_application()
