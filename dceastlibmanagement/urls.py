from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('homepage.urls'),name='homepage'),
    path('books/',include('books.urls'),name='books'),
    path('authors/',include('authors.urls'),name='authors'),
    path('publisher/',include('publisher.urls'),name='publisher'),
    path('accompanyingmaterials/',include('accompanyingmaterials.urls'),name='accompanyingmaterials'),
    path('vendor/',include('vendor.urls'),name='vendor'),
    path('bindingtypes/',include('bindingtypes.urls'),name='bindingtypes'),
    path('doctypes/',include('doctypes.urls'), name='doctypes'),
    path('collectiontype/',include('collectiontype.urls'),name='collectiontype'),
    path('login/',include('loginpage.urls'), name='login'),
    path('modeofacqisition/',include('modeofacqisition.urls'), name='modeofacqisition'),
    path('members/',include('members.urls'),name='members'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
