from accounts.models import Book, Issue
from django.forms import ModelForm

class BookForm(ModelForm):
	class Meta:
		model = Book
		fields = '__all__'

class IssueForm(ModelForm):
	class Meta:
		model = Issue
		fields = '__all__'