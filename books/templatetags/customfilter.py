from django import template
register = template.Library()

@register.simple_tag
def url_replace(request, field=None, value=None):
    dict_ = request.GET.copy()
    if field != None:
        dict_[field] = value
    return dict_.urlencode()

@register.filter
def csvdownloadurlfix(url):
	splitdata = url.split('?')
	if len(splitdata)<2:
		return url
	return splitdata[1]