from django.shortcuts import render, redirect
from accounts.models import *
from .forms import BookForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from datetime import date
from django.core.paginator import Paginator
import csv

def getcontext():
	context = {
		'authors' : Author.objects.all().order_by('author_name'),
		'accmaterials' : AccompanyingMaterials.objects.all(),
		'vendors' : Vendor.objects.all(),
		'bindingtypes':BindingType.objects.all(),
		'doctypes': DocumentType.objects.all(),
		'typeofcollections' : TypeOfCollection.objects.all(),
		'publishers': Publisher.objects.all(),
		'modeofacquisitions':ModeOfAccuisition.objects.all(),
	}
	return context

def searchbookfunc(books, request):
	if (request.GET.get('author')or request.GET.get('publisher')) or request.GET.get('title') or (request.GET.get('accessionnum')):
		if request.GET.get('title'):
			books = books.filter(title__icontains=request.GET.get('title'))
		if request.GET.get('accessionnum'):
			books = books.filter(accession_number__icontains=request.GET.get('accessionnum'))
		if request.GET.get('publisher'):
			books = books.filter(publisher__pub_name__icontains=request.GET.get('publisher'))
		if request.GET.get('author'):
			books = books.filter(author__author_name__icontains=request.GET.get('author'))
	return books

@login_required(login_url='/login/')
def index(request):
	books = Book.objects.all().order_by('accession_number')
	books = searchbookfunc(books,request)
	if request.GET.get('showreport'):
		return render(request,'books/viewreport.html',{'books':books})
	paginator = Paginator(books, 12)
	page = request.GET.get('page')
	books = paginator.get_page(page)
	context = getcontext()
	context['books'] = context['paginationdata'] = books
	return render(request,'books/index.html',context)

@login_required(login_url='/login/')
def addbook(request):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['last_updated_by'] = request.user
		postrequest['added_by'] = request.user
		form = BookForm(postrequest)
		if form.is_valid():
			form.save()
			return redirect('/books/')
		else:
			return HttpResponse(str(form.errors))
	context = getcontext()
	return render(request,'books/bookform.html',context)

@login_required(login_url='/login/')
def editbook(request,pk):
	if request.method == 'POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['last_updated_by'] = request.user
		postrequest['last_updated'] = date.today()
		postrequest['added_by'] = Book.objects.get(pk=pk).added_by
		form = BookForm(postrequest,instance=Book.objects.get(pk=pk))
		if form.is_valid():
			form.save()
			return redirect('/books/')
		else:
			return HttpResponse(str(form.errors))
	context = getcontext()
	context['book'] = Book.objects.get(pk=pk)
	return render(request,'books/bookform.html',context)

def convertauthor(authors):
	a = ''
	for author in authors:
		a += author.author_name+', '
	return a[:-2]

@login_required(login_url='/login/')
def downloadcsv(request):
	response = HttpResponse(
			content_type='text/csv',
			headers={'Content-Disposition': 'attachment; filename="bookdata.csv"'},
		)
	writer = csv.writer(response)
	books = Book.objects.all().order_by('accession_number')
	books = searchbookfunc(books,request)
	writer.writerow(('id','ACCESSION NUMBER','TITLE','SUBTITLE','AUTHOR','EDITION','YEAR','PLACE','PUBLSIHER',
		'NUMBER OF VOLUMES','ACCOMPANYING MATERIALS','ITEM PRICE','VENDOR','INVOICE DATE','INVOICE NUMBER',
		'ACCESSION DATE','TYPE OF BINDING','DOC TYPE','LOCATION','TYPE OF COLLECTION','EDITOR','CEASED',
		'CEASE DATE','MODE OF ACCUSITION','REMARKS'))
	for book in books:
		# return HttpResponse()
		writer.writerow((book.pk,book.accession_number,book.title.title(),book.subtitle,
			convertauthor(book.author.all()),book.edition,book.year,book.place, book.publisher,
			book.num_of_vols,book.accompaning_materials,book.item_price,book.vendor,
			book.invoice_date,book.invoice_number,book.accession_date,book.type_of_binding,
			book.doc_type,book.location,book.type_of_collection,book.editor,
			book.ceased,book.ceases_exist_date,	book.mode_of_accuisition,book.remarks))
	return response