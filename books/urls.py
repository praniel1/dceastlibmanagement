from django.urls import path
from .import views

app_name='books'

urlpatterns = [
	path('',views.index, name='index'),
	path('addbook', views.addbook, name='addbook'),
	path('editbook/<int:pk>', views.editbook, name='editbook'),
	path('downloadcsv',views.downloadcsv,name='downloadcsv'),
]