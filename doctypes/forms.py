from django.forms import ModelForm
from accounts.models import DocumentType

class DocumentTypeForm(ModelForm):
	class Meta:
		model = DocumentType
		fields = '__all__'