from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import DocumentTypeForm
from accounts.models import DocumentType
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def index(request):
	if request.method == 'POST':
		doctype = DocumentType.objects.get(pk=request.POST['pk'])
		doctype.doctype_name = request.POST['name']
		doctype.save()
		return redirect('/doctypes')
	doctypes = DocumentType.objects.all().order_by('-pk')
	return render(request,'doctypes/index.html',{'doctypes':doctypes})

@login_required(login_url='/login/')
def doctypeadd(request):
	if request.method=='POST':
		postrequest = request.POST
		postrequest._mutable = True
		postrequest['added_by'] = request.user
		form = DocumentTypeForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/doctypes')
		else:
			return HttpResponse(form.errors)
	return render(request,'doctypes/doctypeadd.html')